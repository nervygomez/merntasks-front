import React, {useReducer} from 'react';
import alertarReducer from './alertaReducer';
import alertaContenxt from './alertaContext';

import {MOSTRAR_ALERTA, OCULTAR_ALERTA} from '../../types'

const AlertaState = props => {
    const initialState = {
        alerta: null
    }

    const [state, dispatch] = useReducer(alertarReducer, initialState);

    // funciones
    const mostrarAlerta = (msg, categoria) => {
        dispatch({
            type: MOSTRAR_ALERTA,
            payload: {
                msg,
                categoria
            }
        });
        setTimeout(() => {
            dispatch({
                type: OCULTAR_ALERTA
            })
            
        }, 3500);
    }

    return (
        <alertaContenxt.Provider
            value={{
                alerta: state.alerta,
                mostrarAlerta
                
            }}
        >  
            {props.children}
        </alertaContenxt.Provider>
    )
}

export default AlertaState;